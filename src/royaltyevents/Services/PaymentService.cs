﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Data;
using royaltyevents.Models;

namespace royaltyevents.Services
{
    public class PaymentService : IPaymentService
    {
        ApplicationDbContext db;
        public PaymentService(ApplicationDbContext context)
        {
            db = context;
        }
        public bool BookingHasPayment(int id)
        {
            //Check if the booking has a valid payment
           return  db.Payments.Any(x => x.paying_for == PaymentType.booking && x.payedforid == id && x.validuntil>=DateTime.Now);

        }

        public bool ProviderHasPayment(int id)
        {
            //check if the provider has any dservice with a valid payment
            bool ispayed = db.Payments.Any(x => x.paying_for == PaymentType.service && x.service.provider.id == id && x.validuntil >= DateTime.Now);
            return ispayed;
        }

        public bool ServiceHasPayment(int id)
        {
            //check if the service has a valid payment
            return db.Payments.Any(x => x.paying_for == PaymentType.service && x.payedforid == id && x.validuntil >= DateTime.Now);
        }
    }

}
