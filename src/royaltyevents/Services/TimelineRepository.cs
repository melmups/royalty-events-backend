﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Data;
using royaltyevents.Models;
using Microsoft.EntityFrameworkCore;

namespace royaltyevents.Services
{
    public class TimelineRepository : IRepository<TimelineViewModel>
    {
        ApplicationDbContext db;
        IPaymentService payment;
        public TimelineRepository(ApplicationDbContext context, IPaymentService paymentservice)
        {
            payment = paymentservice;
            db = context;
        }
        public void Add(TimelineViewModel timelineModel)
        {

            //retriving sender provider entity
            Provider provider = db.Providers.FirstOrDefault(x=>x.id==timelineModel.provider_id);
            //retrieving sender user entity
            
            //add new timeline to the thread
            Thread thread = new Thread
            {
                 title = timelineModel.title,
                 content = timelineModel.content,
                 date = DateTime.Now,
                 image_url = timelineModel.imageurl,
                 video_url = timelineModel.videourl,
                 type = ThreadType.timeline,
                 sender_provider = provider, 
             };
            db.Threads.Add(thread);
            db.SaveChanges();
        }

        public void AddRange(IEnumerable<TimelineViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public void Delete(TimelineViewModel entity)
        {
            //Remove Timeline thread from id attained in the id
            throw new NotImplementedException();
        }

        public void DeleteRange(IEnumerable<TimelineViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public TimelineViewModel Get(int id)
        {
            //fetch threads of type timeline with the provided id as the sender id
            throw new NotImplementedException();
        }

        public IEnumerable<TimelineViewModel> GetAll()
        {

            throw new NotImplementedException();
        }

        public IEnumerable<TimelineViewModel> GetAll(int id)
        {
            //int providerid;
            //bool isprovider = int.TryParse(id, out providerid);

            //get threads where the user is a follower of the provider who owns the service
            //ensure that the followed provider has at least one service with a valid payment
            

            IList<Thread> threads = db.Threads.Where(x=>x.type== ThreadType.timeline).Include(x=>x.sender_provider).ToList();
            List<TimelineViewModel> timeline = new List<TimelineViewModel>();
            foreach (var thread in threads)
            {

                //check if the provider has any payed service
                bool frompayedprovider = payment.ProviderHasPayment(thread.sender_provider.id);
                //Check if the thread belong to followed provider
                bool isfollowed = db.Followings.Any(x => x.provider.id == thread.sender_provider.id && x.userid==Convert.ToString(id));

                if(frompayedprovider&&isfollowed)
                {
                    //Populate the TimelineViewModel
                    TimelineViewModel timelinethread = new TimelineViewModel
                    {
                        id = thread.id,
                        avator = thread.sender_provider.banner_url,
                        providername = thread.sender_provider.name,
                        provider_id = thread.sender_provider.id,
                        time = thread.date.ToString(),
                        title = thread.title,
                        content = thread.content,
                        imageurl = thread.image_url,
                        videourl = thread.video_url
                    };
         
                    timeline.Add(timelinethread);
                }
                else
                {
                    //threads.Remove(thread);
                }
           
            }
            
            return timeline;
        }

      

        public IEnumerable<TimelineViewModel> GetFromSearch(string query)
        {

            throw new NotImplementedException();
        }

        public void Update(int id)
        {
            throw new NotImplementedException();
        }
    }
}
