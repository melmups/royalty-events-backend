﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace royaltyevents.Services
{
    public interface IRepository<T> where T : class
    {
        T Get(int id);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(int id);

        IEnumerable<T> GetFromSearch(string searchstring);

        void Add(T entity);
        void AddRange(IEnumerable<T> entities);

        void Update(int id);

        void Delete(T entity);
        void DeleteRange(IEnumerable<T> entities);

    }
}
