﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Models;
using royaltyevents.Data;
using Microsoft.EntityFrameworkCore;


namespace royaltyevents.Services
{
    public class UserProfileRepository : IRepository<UserProfileViewModel>
    {
        ApplicationDbContext db;
        IThreadService Threads;
        public UserProfileRepository(ApplicationDbContext context, IThreadService threads)
        {
            db = context;
            Threads = threads;
        }
   
        public void Add(UserProfileViewModel userProfile)
        {
            //Creating a new user profile

            ApplicationUser user = new ApplicationUser
            {
                UserName = userProfile.name,
                //phone = userProfile.phone,
                //Email = userProfile.email,
                country = userProfile.country,
                avator_url = userProfile.avator_url,
                reg_date = DateTime.Now,
                is_client = userProfile.is_client,
                is_provider = userProfile.is_provider,
                is_admin = userProfile.is_admin,
            };

            //creating a provider profile if the user is aprovider

            if(userProfile.is_provider==true)
            {
                Provider provider = new Provider
                {
                    banner_url = userProfile.bannerurl,
                    city = userProfile.city,
                    email = userProfile.email,
                    site_url = userProfile.siteurl,
                    reg_date = DateTime.Now,
                    country = userProfile.country,
                    street_address = userProfile.street_address,
                    province = userProfile.province,
                    field = userProfile.field,
                    phone = userProfile.phone,
                    intro_video_url = userProfile.intro_video_url,
                };
                user.provider = provider;
            }
            db.Users.Add(user);
            db.SaveChanges();
        }

        public void AddRange(IEnumerable<UserProfileViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public void Delete(UserProfileViewModel entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteRange(IEnumerable<UserProfileViewModel> entities)
        {
            throw new NotImplementedException();
        }
        //PERSONAL PROFILE////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////PERSONAL PROFILE
            public UserProfileViewModel Get(int userid)
            {
                var user = db.Users.Include(x=>x.provider).FirstOrDefault(x => x.Id == Convert.ToString(userid));
                UserProfileViewModel profile = new UserProfileViewModel();

                if (user.is_provider==true)
                {

                    // check if profile setup is complete

                    profile.providerid = user.provider.id;
                    profile.name = user.UserName;
                    profile.is_provider = user.is_provider;
                    profile.bannerurl = user.provider.banner_url;
                    profile.intro_video_url = user.provider.intro_video_url;
                    profile.email = user.provider.email;
                    profile.phone = user.provider.phone;
                    profile.city = user.provider.city;
                    profile.province = user.provider.province;
                    profile.country = user.provider.country;
                    profile.bgurl = user.provider.bg_url;
                    profile.siteurl = user.provider.site_url;
                    profile.field = user.provider.field;
                    profile.facebook = user.provider.facebook;
                    profile.instagram = user.provider.instagram;
                    profile.pintrest = user.provider.pintrest;
                    profile.twitter = user.provider.twitter;
                    profile.primary_color = user.provider.primary_color;
                    profile.secondary_color = user.provider.secondary_color;
                
                    /*
                     * count of services
                     * count of clients
                     * count of views
                     * count of likes
                     * count of followers
                     */

                    profile.servicescount = db.Service.Where(x => x.provider.id == user.provider.id).Count();
                    profile.views = db.Views.Where(x => x.provider.id == user.provider.id).Count();
                    profile.likes = db.Likes.Where(x => x.provider.id == user.provider.id).Count();
                    profile.followers = db.Followings.Where(x => x.provider.id == user.provider.id).Count();
                    profile.clientscount = db.Bookings.Where(x => x.service.provider.id == user.provider.id).Count();

                    //Getting the users coments
                    profile.comments = Threads.GetComments(user.provider.id).ToList();
               
                    //Getting the users notifiactions
                    profile.notifications = Threads.GetNotifications(Convert.ToString(user.provider.id)).ToList();
                
                    //Getting the users messages
                    profile.messages = Threads.GetMessages(Convert.ToString(user.provider.id)).ToList();

                    return profile;
                
                }
                else if(user.is_client==true && (user.is_provider==false || user.is_provider==null))
                {
                    //determine if user has a valid event
            
                    profile.id = user.Id;
                    profile.name = user.UserName;
                    profile.is_client = user.is_client;
                    profile.avator_url = user.avator_url;
                    profile.email = user.Email;
                    profile.phone = user.phone;
                    profile.country = user.country;

                    /*
                     * count of bookings
                     */

                    profile.bookingscount = db.Bookings.Where(x => x.occussion.user.Id==Convert.ToString(userid)).Count();

                    //fetching latest event colors
                    profile.primary_color = db.Events.LastOrDefault(x => x.user.Id == user.Id).primary_color;
                    profile.secondary_color = db.Events.LastOrDefault(x => x.user.Id == user.Id).secondary_color;

                    //Getting the users notifiactions
                    profile.notifications = Threads.GetNotifications(Convert.ToString(user.Id)).ToList();

                    //Getting the users messages
                    profile.messages = Threads.GetMessages(Convert.ToString(user.Id)).ToList();




                }
                else
                {
                
                }
                return profile;

            }
        //END PERSONAL PROFILE////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////END PERSONAL PROFILE


        //PROVIDERS PROFILES////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////PROVIDERS PROFILES
        public IEnumerable<UserProfileViewModel> GetAll()
        {
            //check provider profile if it has any valid service payment
            //get only the providers info as user profiles to display to exploring pages
            var providers = db.Providers.ToList();

            //A list to add each populated profile from different tables in the database
            List<UserProfileViewModel> providers_list = new List<UserProfileViewModel>();
            foreach (var provider in providers)
            {

                UserProfileViewModel profile = new UserProfileViewModel()
                {
                    
                    providerid = provider.id,
                    name = provider.name,
                    bannerurl = provider.banner_url,
                    intro_video_url = provider.intro_video_url,
                    email = provider.email,
                    phone = provider.phone,
                    city = provider.city,
                    province = provider.province,
                    country = provider.country,
                    bgurl = provider.bg_url,
                    siteurl = provider.site_url,
                    field = provider.field,
                    facebook = provider.facebook,
                    instagram = provider.instagram,
                    pintrest = provider.pintrest,
                    twitter = provider.twitter,
                    primary_color = provider.primary_color,
                    secondary_color = provider.secondary_color,
                };
                /*
                 * count of services
                 * count of clients
                 * count of views
                 * count of likes
                 * count of followers
                 */

                profile.servicescount = db.Service.Where(x => x.provider.id == provider.id).Count();
                profile.views = db.Views.Where(x => x.provider.id == provider.id).Count();
                profile.likes = db.Likes.Where(x => x.provider.id == provider.id).Count();
                profile.followers = db.Followings.Where(x => x.provider.id == provider.id).Count();
                profile.clientscount = db.Bookings.Where(x => x.service.provider.id == provider.id).Count();

                //Getting the users coments
                profile.comments = Threads.GetComments(profile.providerid).ToList();
                          
                providers_list.Add(profile);
            }
            return providers_list;
        }
        //END PROVIDERS PROFILES////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////END PROVIDERS PROFILES

        public IEnumerable<UserProfileViewModel> GetAll(int id)
        {
            
            throw new NotImplementedException();
        }

        public IEnumerable<UserProfileViewModel> GetFromSearch(string searchstring)
        {
            //Search the providers userprofiles by searchstring
            throw new NotImplementedException();
        }

        public void Update(int id)
        {
            throw new NotImplementedException();
        }
    }
}
