﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Models;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Data;

namespace royaltyevents.Services
{
    public class PaymentRepository : IRepository<PaymentViewModel>
    {
        ApplicationDbContext db;
        public PaymentRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public void Add(PaymentViewModel paymentviewmodel)
        {
            //Add the payment entity from payment view
            Payment payment = new Payment()
            {
                amount = paymentviewmodel.Amount,
                date = DateTime.Now,
                payedforid = paymentviewmodel.payedforid
            };

            if (paymentviewmodel.paying_for == "service")
            {
                payment.paying_for = PaymentType.service;
                //look for the service we are paying for
                Service service = db.Service.FirstOrDefault(x => x.id == paymentviewmodel.payedforid);
                if(service!=null)
                {
                    payment.service = service;
                }
                //calculating valid until
                //just add 30 days from now
                payment.validuntil = DateTime.Now.AddDays(30);
            }
            db.Payments.Add(payment);
            db.SaveChanges();

        }

        public void AddRange(IEnumerable<PaymentViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public void Delete(PaymentViewModel entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteRange(IEnumerable<PaymentViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public PaymentViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public PaymentViewModel Get(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PaymentViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PaymentViewModel> GetAll(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PaymentViewModel> GetAll(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PaymentViewModel> GetFromSearch(string searchstring)
        {
            throw new NotImplementedException();
        }

        public void Update(int id)
        {
            throw new NotImplementedException();
        }
    }
}
