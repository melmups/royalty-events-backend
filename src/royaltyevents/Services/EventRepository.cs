﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Models;
using royaltyevents.Data;
using Microsoft.EntityFrameworkCore;

namespace royaltyevents.Services
{
    class EventRepository : IRepository<EventViewModel>

    {
        ApplicationDbContext db;
        public EventRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public void Add(EventViewModel schedule)
        {

            //Look for user with the id
            ApplicationUser User = db.Users.FirstOrDefault(x => x.Id == schedule.userid);
            //Look for eventtype id
            EventType eventtype = db.EventTypes.FirstOrDefault(x => x.name == schedule.eventType);
            //determining if its final event;
            if(schedule.isfinalEvent==true)
            {
                //Create the final event
                Event occussion = new Event()
                {
                    name = schedule.title,
                   
                    description = schedule.description,

                    user = User,
                    venue = schedule.venue,
                    event_type = eventtype,
                    start_date = schedule.start,
                    end_date = schedule.end
               };
                try
                {
                    occussion.primary_color = schedule.color.primary;
                    occussion.secondary_color = schedule.color.secondary;
                }
                catch (Exception)
                {    
                }
                    
           

                db.Events.Add(occussion);
                
            }
            else
            {
                //Lookup for the occussion to attach the schedule to
                var occ = db.Events.FirstOrDefault(x => x.id == schedule.finalEventId);
                //Create the schedule for the final event
                Schedule sched = new Schedule()
                {
                    title = schedule.title,
                    start_date = Convert.ToDateTime(schedule.start),
                    end_date = Convert.ToDateTime(schedule.end),
                    occussion = occ,
                    description = schedule.description,

                };
                try
                {
                    sched.primary_color = schedule.color.primary;
                    sched.secondary_color = schedule.color.secondary;
                }
                catch (Exception){}

                db.Schedules.Add(sched);

            }
            db.SaveChanges();
        }

        public void AddRange(IEnumerable<EventViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public void Delete(EventViewModel entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteRange(IEnumerable<EventViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public EventViewModel Get(int id)
        {
            throw new NotImplementedException();

        }

        public IEnumerable<EventViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EventViewModel> GetAll(int id)
        {
            var sid = Convert.ToString(id);
            List<EventViewModel> schedules = new List<EventViewModel>();
            //take all events and their schedules that belong to them as user id or their fellow collegues and their client's shared schedule
            int providerid = 0;
            var provider = db.Providers.FirstOrDefault(x => x.users.Any(l => l.Id == sid));
            if (provider != null)
            {
                providerid = provider.id;
            }
            IEnumerable<Event> events = db.Events.Where(x => x.user.Id == sid || x.user.provider.id == providerid).Include(X => X.schedules).Include(x=>x.user);
            foreach (var occussion in events)
            {
                //convet the event to eventview model of a fixed schedule non dragable final event 
                EventViewModel eventEventView = new EventViewModel()
                {
                    id = occussion.id,
                    title = occussion.name,
                    description = occussion.description,
                    start = occussion.start_date,
                    end = occussion.end_date,
                    isfinalEvent = true,
                    finalEventId = occussion.id,
                    finalEventName = occussion.name,
                    userid = occussion.user.Id,
                    color = new Eventcolor() { primary = "#1e90ff", secondary = "#D1E8FF" },
                    resizable = new Eventresize() { afterEnd = false, beforeStart = false },
                };
                schedules.Add(eventEventView);
                foreach (var schedule in occussion.schedules)
                {
                
                    //convet the schedules to event view model draggable
                    var scheduleEventView = new EventViewModel()
                    {
                        id = schedule.id,
                        title = schedule.title,
                        start = schedule.start_date,
                        end = schedule.end_date,
                        isfinalEvent = false,
                        finalEventId = occussion.id,
                        finalEventName = occussion.name,
                        color = new Eventcolor(){primary = "#e3bc08",secondary = "#FDF1BA"},
                        description = schedule.title,
                        resizable =  new Eventresize() {afterEnd = true, beforeStart = true}
                    };
                    schedules.Add(scheduleEventView);
                }
               
            }
            return schedules;
        }

        public IEnumerable<EventViewModel> GetFromSearch(string searchstring)
        {
            throw new NotImplementedException();
        }

        public void Update(int id)
        {
            throw new NotImplementedException();
        }
    }
}
