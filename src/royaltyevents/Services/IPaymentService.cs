﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace royaltyevents.Services
{
   public interface IPaymentService
    {
        bool BookingHasPayment(int id);
        bool ServiceHasPayment(int id);
        bool ProviderHasPayment(int id);
        

        
    }
}
