﻿using royaltyevents.Models.ClientViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace royaltyevents.Services
{
    public interface IThreadService
    {
        IEnumerable<Comment> GetComments(int provider_id);
        IEnumerable<Message> GetMessages(string id);
        IEnumerable<Notification> GetNotifications(string id);

    }
}