﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Data;
using royaltyevents.Models;
using Microsoft.EntityFrameworkCore;

namespace royaltyevents.Services
{
    public class ServiceRepository : IRepository<ServiceViewModel>
    {
        ApplicationDbContext db;
        public IPaymentService paymentservice;
        public ServiceRepository(ApplicationDbContext context, IPaymentService payment)
        {
            paymentservice = payment;
            db = context;
        }

       
        public void Add(ServiceViewModel servicemodel)
        {

            //Retrieve the provider
            Provider provider = db.Providers.FirstOrDefault(x => x.id == servicemodel.provider_id);
            // Creating a new serviceentity from view model
            Service service = new Service()
            {
                title = servicemodel.title,
                description = servicemodel.description,
                deposit = servicemodel.deposit,
                price = servicemodel.price,
                name = servicemodel.title,
                terms_and_conditions = servicemodel.terms_and_conditions,
                disclaimer = servicemodel.disclaimer,
                image1_id = Convert.ToInt32(servicemodel.image1),
                image2_id = Convert.ToInt32(servicemodel.image2),
                image3_id = Convert.ToInt32(servicemodel.image3),
                image4_id = Convert.ToInt32(servicemodel.image4),
                image5_id = Convert.ToInt32(servicemodel.image5),
                image6_id = Convert.ToInt32(servicemodel.image6),
                provider = provider                  
            };
            //Looking for categoty id
            
            db.Service.Add(service);
            db.SaveChanges();

        }

        public void AddRange(IEnumerable<ServiceViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public void Delete(ServiceViewModel entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteRange(IEnumerable<ServiceViewModel> entities)
        {
            throw new NotImplementedException();
        }



        public ServiceViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ServiceViewModel> GetAll()
        {

            throw new NotImplementedException();
        }

        public IEnumerable<ServiceViewModel> GetAll(int id)
        {
            bool ispersonal = false;
            //Check if the current user is the owner of the service;

            //int provider_id;
            //bool is_provider = int.TryParse(id, out provider_id);
            //if(!is_provider)
            //{
            //    ispersonal = true;
            //    var user = db.Users.Include(x=>x.provider).FirstOrDefault(x => x.Id ==Convert.ToString(id);
            //    provider_id = user.provider.id;
            //}
         
            //fetch services according to provider id
            //chech each service and determine if it has a valid payment
            //populate the ServiceViewModel
            //determine Action1 and Action2 depending on the payment of each service
            IList<Service> dbservices = db.Service.Where(x => x.provider.id == id).Include(x=>x.provider).ToList();
            List<ServiceViewModel> services = new List<ServiceViewModel>();
            foreach (Service dbservice in dbservices)
            {
               var serv = dbservice;
                //Construct the service view model form entity
                if ((paymentservice.ServiceHasPayment(dbservice.id) && !ispersonal) || ispersonal)
                {
                    ServiceViewModel service = new ServiceViewModel()
                    {
                        id = dbservice.id,
                        provider_id = dbservice.provider.id,
                        provider_name = dbservice.provider.name,
                        title = dbservice.title,
                        description = dbservice.description,
                        deposit = dbservice.deposit,
                        price = dbservice.price,
                    };

                    service.image1 = db.Gallery.FirstOrDefault(x => x.id == dbservice.image1_id).file_url;
                    service.image2 = db.Gallery.FirstOrDefault(x => x.id == dbservice.image2_id).file_url;
                    service.image3 = db.Gallery.FirstOrDefault(x => x.id == dbservice.image3_id).file_url;
                    service.image4 = db.Gallery.FirstOrDefault(x => x.id == dbservice.image4_id).file_url;
                    service.image5 = db.Gallery.FirstOrDefault(x => x.id == dbservice.image5_id).file_url;
                    service.image6 = db.Gallery.FirstOrDefault(x => x.id == dbservice.image6_id).file_url;

                    //Determining actions 1 and 2
                    //If the service has a valid 
                    if (ispersonal)
                    {
                        if (paymentservice.ServiceHasPayment(dbservice.id))
                        {
                            service.action1 = "Update";
                        }
                        else
                        {
                            service.action1 = "PayFor";
                        }
                        service.action2 = "Remove";
                    }
                    else
                    {
                        service.action1 = "Book";
                        service.action2 = "Follow";
                    }
                    services.Add(service);
                }
                else
                {

                }
            }
            return services;
        }

        public IEnumerable<ServiceViewModel> GetFromSearch(string searchstring)
        {
            throw new NotImplementedException();
        }

        public void Update(int id)
        {
            throw new NotImplementedException();
        }
  
    }
  

}
  


