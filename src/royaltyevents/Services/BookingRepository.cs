﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Data;
using royaltyevents.Models;
using Microsoft.EntityFrameworkCore;

namespace royaltyevents.Services
{
    public class BookingRepository : IRepository<BookingViewModel>
    {
        ApplicationDbContext db;
        IPaymentService payment;
        public BookingRepository(ApplicationDbContext context, IPaymentService paymentservice)
        {
            payment = paymentservice;
            db = context;
        }
        public void Add(BookingViewModel bookingview)
        {
            //
            Event occurssion = db.Events.FirstOrDefault(x => x.id == bookingview.eventid);
            Service sevice = db.Service.FirstOrDefault(x => x.id == bookingview.serviceid);
            //create and save booking entity
            Booking booking = new Booking
            {
                stage = BookingStage.request,
                occussion = occurssion,
                service = sevice,
                specs = bookingview.description
            };

            db.Bookings.Add(booking);
            db.SaveChanges();

        }

        public void AddRange(IEnumerable<BookingViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public void Delete(BookingViewModel entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteRange(IEnumerable<BookingViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public BookingViewModel Get(string id)
        {
            throw new NotImplementedException();
        }

        public BookingViewModel Get(int id)
        {
            var bkng = db.Bookings.FirstOrDefault();
            BookingViewModel booking = new BookingViewModel
            {
                title = "sample booking",
                id = id,
                provider = "Rollblights",
                eventname ="the Patier"
            };
            return booking;
        }

        public IEnumerable<BookingViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BookingViewModel> GetAll(int id)
        {
            var sid = Convert.ToString(id);
            //fetch bookings using client id
            //get bookings with valid events
            //dtermine action1 and action2 depending on payment and provider approval
            //check event validity
            IEnumerable<Booking> dbbookings = db.Bookings.Where(x => x.occussion.user.Id == sid && x.occussion.start_date > DateTime.Now)
                .Include(x=>x.occussion).Include(x=>x.service).Include(x=>x.service.provider).Include(x => x.occussion.user).ToList();
            List<BookingViewModel> bookings = new List<BookingViewModel>();
            foreach (Booking dbbooking in dbbookings)
            {
                BookingViewModel booking = new BookingViewModel()
                {
                    id = dbbooking.id,
                    userid = dbbooking.occussion.user.Id,
                    title = dbbooking.service.title,
                    description = dbbooking.service.description,
                    eventid = dbbooking.occussion.id,
                    eventname = dbbooking.occussion.name,
                    provider = dbbooking.service.provider.name,
                    serviceid = dbbooking.service.id,
                    deposit = dbbooking.service.deposit,
                    price = dbbooking.service.price,
                };
                //Determine action 1 and action 2
                if(dbbooking.stage == BookingStage.approved)
                {
                    if (payment.BookingHasPayment(dbbooking.id))
                    {
                        booking.action1 = "Add";
                    }
                    else
                    {
                        booking.action1 = "Schedule";
                    }
                    booking.action2 = "Remove";
                }
                else
                {
                    booking.action1 = "pending";
                    booking.action2 = "Cancel";
                }
                //populating images
                booking.image1 = db.Gallery.FirstOrDefault(x => x.id == dbbooking.service.image1_id).file_url;
                booking.image2 = db.Gallery.FirstOrDefault(x => x.id == dbbooking.service.image2_id).file_url;
                booking.image3 = db.Gallery.FirstOrDefault(x => x.id == dbbooking.service.image3_id).file_url;
                booking.image4 = db.Gallery.FirstOrDefault(x => x.id == dbbooking.service.image4_id).file_url;
                booking.image5 = db.Gallery.FirstOrDefault(x => x.id == dbbooking.service.image5_id).file_url;
                booking.image6 = db.Gallery.FirstOrDefault(x => x.id == dbbooking.service.image6_id).file_url;

                bookings.Add(booking);
            }
            return bookings;
        }

        public IEnumerable<BookingViewModel> GetFromSearch(string searchstring)
        {
            throw new NotImplementedException();
        }

        public void Update(int id)
        {
            throw new NotImplementedException();
        }
    }
}
