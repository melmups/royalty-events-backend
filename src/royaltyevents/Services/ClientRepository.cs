﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Data;
using royaltyevents.Models;
using Microsoft.EntityFrameworkCore;

namespace royaltyevents.Services
{
    public class ClientRepository : IRepository<ClientsViewModel>
    {
        ApplicationDbContext db;
        public ClientRepository(ApplicationDbContext dbcontext)
        {
            db = dbcontext;
        }
        public void Add(ClientsViewModel entity)
        {
            throw new NotImplementedException();
        }

        public void AddRange(IEnumerable<ClientsViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public void Delete(ClientsViewModel entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteRange(IEnumerable<ClientsViewModel> entities)
        {
            throw new NotImplementedException();
        }

        public ClientsViewModel Get(string id)
        {
            throw new NotImplementedException();
        }

        public ClientsViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ClientsViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ClientsViewModel> GetAll(int id)
        {
            //int providerid;
            //bool isprovider = int.TryParse(id, out providerid);

            //Using provider_id toget fetch the bookings whicgh involve the provider and then 
            //Populating the client profile
            //client information

            List<Booking> bookings = db.Bookings.Where(x => x.service.provider.id == id).Include(x=>x.occussion).Include(x=>x.occussion.user).ToList();
            //Determine the if the client have pending bookings
            //populate client's bookings 

            List<ClientsViewModel> clients = new List<ClientsViewModel>();
            foreach (var booking in bookings)
            {
                ClientsViewModel client = new ClientsViewModel
                {
                    id = booking.occussion.user.Id,
                    name = booking.occussion.user.UserName,
                    phone = booking.occussion.user.phone,
                    email = booking.occussion.user.Email,
                    city = booking.occussion.user.country,
                    avator = booking.occussion.user.avator_url,

                    eventname = booking.occussion.name,
                    province = booking.occussion.venue

                };
                //calculating days left
                client.daysleft = (int)(booking.occussion.start_date - DateTime.Now).TotalDays;

                clients.Add(client);
            }
            return clients;
        }

        public IEnumerable<ClientsViewModel> GetFromSearch(string searchstring)
        {
            throw new NotImplementedException();
        }

        public void Update(int id)
        {
            throw new NotImplementedException();
        }
    }
}
