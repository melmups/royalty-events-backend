﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Data;
using royaltyevents.Models;
using Microsoft.EntityFrameworkCore;

namespace royaltyevents.Services
{
    public class ThreadServices: IThreadService
    {
        ApplicationDbContext db;
        public ThreadServices(ApplicationDbContext context)
        {
            db = context;
        }


        public IEnumerable<Comment> GetComments(int provider_id)
        {
 
            //Getting the users coments
            ThreadType comment = ThreadType.comment;
            var cmnt_threads = db.Threads.Where(x => x.type == comment &&  x.target_providerid == provider_id).Include(x => x.sender_provider).Include(x => x.sender_user).ToList();

            List<Comment> comments = new List<Comment>();
            foreach (var cmnt_thread in cmnt_threads)
            {
                var singlecomment = new Comment()
                {
                    id = cmnt_thread.id,
                    avator_url = cmnt_thread.sender_user.avator_url,
                    name = cmnt_thread.sender_user.UserName,
                    content = cmnt_thread.content,
                    time = cmnt_thread.date,
                };
                comments.Add(singlecomment);
                
            }
            return comments;
        } 
       public  IEnumerable<Message> GetMessages(string id)
        {
            int provider_id;
            bool isProvider = int.TryParse(id, out provider_id);
            string user_id = "";

            if (!isProvider)
            {
                user_id = id;
            }

            ThreadType message = ThreadType.massage;
            var notif_threads = db.Threads.Where(x => x.type == message && (x.target_providerid == provider_id || x.target_userid == user_id)).Include(x=>x.sender_provider).Include(x=>x.sender_user).ToList();

            List<Message> messeges = new List<Message>();
            foreach(var thread  in notif_threads)
            {
                Message singlemessage = new Message()
                {
                    content = thread.content,
                    time = thread.date,
                };
                if (thread.sender_provider != null)
                {
                    singlemessage.sender_providerid = thread.sender_provider.id;
                    singlemessage.avator_url = thread.sender_provider.banner_url;
                    singlemessage.name = thread.sender_provider.name;
                }
                else
                {
                    singlemessage.sender_userid = thread.sender_user.Id;
                    singlemessage.avator_url = thread.sender_user.avator_url;
                    singlemessage.name = thread.sender_user.UserName;
                }
                //determine if the message is already read
                bool is_read = db.Recepients.Any(x => x.thread.id == thread.id && (x.providerid == provider_id || x.userid == user_id));
                singlemessage.is_read = is_read;
                messeges.Add(singlemessage);
            }
            return messeges;
        }                                                                                                                                


        public IEnumerable<Notification> GetNotifications(string id)
        {
            int provider_id;
            bool isProvider = int.TryParse(id, out provider_id);
            string user_id = "";

                if (!isProvider)
                {
                    user_id = id;
                }

                ThreadType notification = ThreadType.notification;
                var notif_threads = db.Threads.Where(x => x.type == notification && (x.target_providerid == provider_id||x.target_userid == user_id)).Include(x=>x.sender_provider).Include(x=>x.sender_provider).ToList();

                List<Notification> notifications = new List<Notification>();

                foreach (var notif_thread in notif_threads)
                {
                    var singlenotification = new Notification()
                    {
                        id = notif_thread.id,
                        title = notif_thread.content,
                         picture_url = notif_thread.image_url            
                    };
                    if (notif_thread.sender_provider!=null)
                    {
                        singlenotification.sender_providerid = notif_thread.sender_provider.id;
                        singlenotification.avator_url = notif_thread.sender_provider.banner_url;
                        singlenotification.name = notif_thread.sender_provider.name;
                    }
                    else
                    {
                        singlenotification.sender_userid = notif_thread.sender_user.Id;
                        singlenotification.avator_url = notif_thread.sender_user.avator_url;
                        singlenotification.name = notif_thread.sender_user.UserName;
                    }
                  

                    //Checking if the Notification is already Read

                    //Adding the notification to the notifications list
                    notifications.Add(singlenotification);
                }
            return notifications;
        }
      
    } 
}
