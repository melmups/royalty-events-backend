﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;


namespace royaltyevents.Models
{

    public enum BotSettingType { entity, intent }
    public enum GalleryType { image,video }
    public enum BookingStage { request, approved , declined, payed }
    public enum PaymentType { service , booking }
    public enum TargetGroup {none, clients, providers }
    public enum ThreadType { notification, massage, sms, timeline, email, comment }
    public enum ClassLevel { basic, standard,premium }

    public class Provider
    {
        public int id { get; set; }
        public string name { get; set; }
        public string banner_url { get; set; }
        public string banner_url_ff { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string street_address { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string country { get; set; }
        public string lat_long { get; set; }
        public string field { get; set; }
        public string site_url { get; set; }
        public string intro_video_url { get; set; }
        public string intro_video_url_ff { get; set; }
        public string radius { get; set; }
        public string reg_no { get;set;}
        public DateTime reg_date { get; set; }
        public string primary_color { get; set; }
        public string secondary_color { get; set; }
        public string bg_url { get; set; }
        public string bgurl_ff { get; set; }
        public string facebook { get; set; }
        public string twitter { get; set; }
        public string pintrest { get; set; }
        public string instagram { get; set; }

        public ICollection<ApplicationUser> users { get; set; }
    }

    public class BotSetting
    {
        public int id { get; set; }
        public BotSettingType bot_setting_type { get; set; }
        public string value { get; set; }
        public string language { get; set; }
        public string exlusions { get; set; }
        public string positions { get; set; }
        public string translation { get; set; }
        public int similar_id { get; set; }
        
    }

    public class EventType
    {
        public int id { get; set; }
        public string name { get; set; }

    }

    public class EventTypePackage
    {
        public int id { get; set; }    
        public string class_level { get; set; }
        public string min_price { get; set; }
        public string max_price { get; set; }

        public virtual EventType event_type { get; set; }
        public virtual ServiceCategory service_category { get; set; }
    }

    public class Event
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string description { get; set; }
        public string primary_color { get; set; }
        public string secondary_color { get; set; }
        public string venue { get; set; }
        public virtual EventType event_type { get; set; }
        public ICollection<Schedule> schedules { get; set; }
        public virtual ApplicationUser user{ get; set; }
    }

    public class Schedule
    {
        public int id { get; set; }
        public string title { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string description { get; set; }
        public string primary_color { get; set; }
        public string secondary_color { get; set; }

        public virtual Event occussion { get; set; }
    }

    public class ServiceCategory
    {
        public int id { get; set; }
        public string name { get; set; }
        public ICollection<Service> services { get; set; }

    }

    public class Service
    {
        public int id { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int image1_id { get; set; }
        public int image2_id { get; set; }
        public int image3_id { get; set; }
        public int image4_id { get; set; }
        public int image5_id { get; set; }
        public int image6_id { get; set; }
        public decimal price { get; set; }
        public decimal deposit { get; set; }
        public string terms_and_conditions { get; set; }
        public string disclaimer { get; set; }

        public virtual ServiceCategory category { get; set; }
        public virtual Provider provider { get; set; }
        public ICollection<Payment> payments { get; set; }
    }
    public class Gallery
    {
        public int id { get; set; }
        public GalleryType type { get; set; }
        public string file_url { get; set; }
        public string ff_file_url { get; set; }
        public virtual Thread thread { get; set; }
    
    }
    public class View
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public virtual Provider provider { get; set; }

    }
    public class Like
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public string userid { get; set; }
        public virtual Provider provider { get; set; }
        

    }
    public class Following
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public string userid { get; set; }
        public virtual Provider provider { get; set; }
     
    }
    public class Booking
    {
        public int id { get; set; }
        public BookingStage stage { get; set; }
        public string specs { get; set; }
        public virtual Event occussion { get; set; }
        public virtual Service service { get; set; }
        
    }
    public class Payment
    {
        public int  id { get; set; }
        public decimal amount { get; set; }
        public PaymentType paying_for { get; set; }
        public DateTime date { get; set; }
        public DateTime validuntil { get; set; }
        public int payedforid { get; set; }

        public virtual Service service { get; set; }

    }
    public class Thread
    {
        public int id { get; set; }
        public ThreadType type { get; set; }
         
        public DateTime date { get; set; }
        public TargetGroup  target_group { get; set; }
        public int? target_providerid { get; set; }
        public string target_userid { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public string image_url { get; set; }
        public string image_url_ff { get; set; }
        public string video_url { get; set; }
        public string video_url_ff { get; set; }

        public virtual ApplicationUser sender_user { get; set; }
        public virtual Provider sender_provider { get; set; }
        public ICollection<Gallery> gallery { get; set; }

    }
    public class Recepient
    {
        public int id { get; set; }
        public string userid { get; set; }
        public int providerid { get; set; }
        public bool is_read { get; set; }
        public virtual Thread thread { get; set; }
    }
 

}