﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace royaltyevents.Models.ClientViewModels
{
    public enum Action1 { pending,add,payfor,update }
    public enum Action2 { cancel, notify, delete, remove }
    public class BookingViewModel:IValidatableObject
    {
        public int id { set; get; }
        public string title { set; get; }
        public string provider { set; get; }
        public string eventname { set; get; }
        [Required]
        [RegularExpression("([0-9]+)")]
        public int eventid { get; set; }
        [Required]
        [RegularExpression("([0-9]+)")]
        public int serviceid { get; set; }
        public string userid { get; set; }
        [DataType(DataType.Currency)]
        public decimal price { set; get; }
        [DataType(DataType.Currency)]
        public decimal deposit { set; get; }
        public string description { set; get; }
        public string action1 { set; get; }
        public string action2 { set; get; }
        public string image1 { get; set; }
        public string image1_ff { get; set; }
        public string image2 { get; set; }
        public string image2_ff { get; set; }
        public string image3 { get; set; }
        public string image3_ff { get; set; }
        public string image4 { get; set; }
        public string image5 { get; set; }
        public string image6 { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> res = new List<ValidationResult>();
            if (serviceid <= 0)
            {
                res.Add(
                new ValidationResult(errorMessage: "providerid is required", memberNames: new[] { "Error-Metadata" }));
            }
            if (eventid <= 0)
            {
                res.Add(
                new ValidationResult(errorMessage: "eventid is required", memberNames: new[] { "Error-Metadata" }));
            }
            return res;
        }
    }

    public class ServiceViewModel: IValidatableObject
    {
        public int id { get; set; }
        [Required]
        [StringLength(25,ErrorMessage = "Title should be in two words or less")]
        public string title { get; set; }
        [Required]
        public int provider_id { get; set; }
        public string provider_name { get;set; }
        [Required]
        public decimal price { get; set; }
        [Required]
        public decimal deposit { get; set; }
        [Required]
        [StringLength(75, ErrorMessage = "Description should be 15 words or less")]
        public string description { get; set; }
        public string action1 { get; set; }
        public string action2 { get; set; }
        public string terms_and_conditions { get; set; }
        public string disclaimer { get; set; }
        [Required]
        public string category { get; set; }
        [Required]
        public string image1 { get; set; }
       
        public string image1_ff { get; set; }
        [Required]
        public string image2 { get; set; }

        public string image2_ff { get; set; }
        [Required]
        public string image3 { get; set; }
    
        public string image3_ff { get; set; }
        [Required]
        public string image4 { get; set; }
        [Required]
        public string image5 { get; set; }
        [Required]
        public string image6 { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> res = new List<ValidationResult>();
            if (provider_id <= 0)
            {
                res.Add(
                new ValidationResult(errorMessage: "provider_id is required", memberNames: new[] { "Error-Metadata" }));
            }
            return res;
        }
    }

    public class PaymentViewModel:IValidatableObject
    {
        public int id { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }
        public string paying_for { get; set; }
        public DateTime date { get; set; }
        public DateTime validuntil { get; set; }
        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage ="paidforid should be a number")]
        public int payedforid { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> res = new List<ValidationResult>();
            if (payedforid <= 0)
            {
                res.Add(
                new ValidationResult(errorMessage: "providerid is required", memberNames: new[] { "Error-Metadata" }));
            }
            if (Amount <= 0)
            {
                res.Add(
                new ValidationResult(errorMessage: "Invalid amount ", memberNames: new[] { "Error-Metadata" }));
            }

            return res;
        }
    }

    public class ClientsViewModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string eventname { get; set; }
        public string avator { get; set; }
        public string avator_ff { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int daysleft { get; set; }
    }
 

    public class EventViewModel : IValidatableObject
    {
            public int id { get; set; }
            [Required]
            public DateTime start { get; set; }
            [Required]
            public DateTime end { get; set; }
            [Required]
            public string title { get; set; }
            public string description { get; set; }
            public bool isfinalEvent { get; set; }

            [RegularExpression("([0-9]+)", ErrorMessage = "paidforid should be a number")]
            public int finalEventId { get; set; }
            public string finalEventName { get; set; }
            [Required]
            public string userid { get; set; }
            public string venue { get; set; }
            public string eventType { get; set; }
            public Eventcolor color { get; set; }
            public  Eventresize resizable { get; set; }

            public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
            {
                List<ValidationResult> res = new List<ValidationResult>();
                if (end < start)
                {
                    res.Add(
                    new ValidationResult(errorMessage: "EndDate must be greater than StartDate",
                    memberNames: new[] { "EndDate" }));
                }
                if (isfinalEvent !=true && finalEventId <= 0)
                {
                res.Add(
                    new ValidationResult(errorMessage: "Either int finalEventId or bool isfinalEvent is required",
                    memberNames: new[] { "EndDate" }));
                }
                if (start<=DateTime.Now)
                {
                    res.Add(
                       new ValidationResult(errorMessage: "start date must be greater than today ",
                       memberNames: new[] { "EndDate" }));
                }

                return res;
            }
    }
        public class Eventcolor
        {
           
            public string primary { get; set; }
          
            public string secondary { get; set; }
        }
        public class Eventresize
        {
            public bool beforeStart { get; set; }
            public bool afterEnd { get; set; }
        }

    public class TimelineViewModel : IValidatableObject
    {
        public int id { get; set; }
        public string time { get; set; }
        public string title { get; set; }
        public string avator { get; set; }
        public string avator_ff { get; set; }
        public string providername { get; set; }

        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "providerid should be a number")]
        public int provider_id { get; set; }
        public string content { get; set; }
        public string imageurl { get; set; }
        public string imageurl_ff { get; set; }
        public string videourl { get; set; }
        public string videourl_ff { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> res = new List<ValidationResult>();
            if (content==null && imageurl==null && videourl ==null)
            {
                res.Add(
                new ValidationResult(errorMessage: "either content or imageurl or videourl is required ", memberNames: new[] { "Error-Content" }));
            }
            if (provider_id<=0)
            {
                res.Add(
                new ValidationResult(errorMessage: "provider_id is required", memberNames: new[] { "Error-Metadata" }));
            }
            return res;
        }
        
    }
    
    public class UserProfileViewModel
    {
        public string id { get; set; }
        public int providerid { get; set; }
        public string name {get;set;}
        public string email {get;set;}
        public string phone {get;set;}
        public string avator_url {get;set;}
        public string avator_url_ff { get; set; }
        
        public bool? is_client { get; set; }
        public bool? is_provider { get; set; }
        public bool? is_admin { get; set; }
        public string bgurl { get; set; }
        public string bgurl_ff { get; set; }
        public string intro_video_url { get; set; }
        public string intro_video_url_ff { get; set; }
        public string bannerurl { get; set; }
        public string bannerurl_ff { get; set; }
        public string usertype {get;set;}
        public string city {get;set;}
        public string province {get;set;}
        public string country { get; set; }
        public string siteurl {get;set;}
        public string facebook {get;set;}
        public string twitter {get;set;}
        public string pintrest {get;set;}
        public string instagram {get;set;}
        public int views {get;set;}
        public int likes {get;set;}
        public int followers {get;set;} 
        public int clientscount {get;set;}
        public int bookingscount {get;set;}
        public int servicescount {get;set;}
        public string street_address { get; set; }
        public string field {get;set;}
        public string primary_color { get; set; }
        public string secondary_color { get; set; }
        public string setup_stage { get; set; }
        public ICollection<Message> messages { get; set; }
        public ICollection<Notification> notifications { get; set; }
        public ICollection<Comment> comments { get; set; }
        public ICollection< EventViewModel> events { get; set; }

    }
    public class Message: IValidatableObject
    {
        public int id { get; set; }
        public DateTime time { get; set; }
        public string name { get; set; }
        public string sender_userid { get; set; }
        public int sender_providerid { get; set; }
        [Required]
        public string content { get; set; }
        public string avator_url { get; set; }
        public string avator_url_ff { get; set; }
        public bool is_read { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> res = new List<ValidationResult>();
            if (sender_userid==null && sender_providerid <= 0)
            {
                res.Add(
                new ValidationResult(errorMessage: "Either sender_provderid or sender_userid is required "));
            }
            return res;
        }
    }
    public class Notification
    {
        public int id { get; set; }
        public string name { get; set; }
        public string avator_url { get; set; }
        public string avator_url_ff { get; set; }
        public string sender_userid { get; set; }
        public int sender_providerid { get; set; }
        public string title { get; set; }
        public string picture_url { get; set; }
        public string picture_url_ff { get; set; }
        public bool is_read { get; set; }

    }
    public class Comment
    {
        public int id { get; set; }
        public DateTime time { get; set; }
        public string name { get; set; }
        [Required]
        public int profileid { get; set; }
        public string avator_url { get; set; }
        public string avator_url_ff { get; set; }
        public string avator_ff { get; set; }
        [Required]
        public string content { get; set; }
    }

}

