﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace royaltyevents.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string surname { get; set; }
        public string avator_url { get; set; }
        public string phone { get; set; }
        public string country { get; set; }
        public bool? is_client { get; set; }
        public bool? is_provider { get; set; }
        public bool? is_suspended { get; set; }
        public bool? is_admin { get; set; }
        public DateTime reg_date { get; set; }

        public virtual Provider provider { get; set; }
        public ICollection<Event> events { get; set; }
    }
}
    