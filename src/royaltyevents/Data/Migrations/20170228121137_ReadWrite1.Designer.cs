﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using royaltyevents.Data;

namespace royaltyevents.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170228121137_ReadWrite1")]
    partial class ReadWrite1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("royaltyevents.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("avator_url");

                    b.Property<string>("country");

                    b.Property<bool?>("is_admin");

                    b.Property<bool?>("is_client");

                    b.Property<bool?>("is_provider");

                    b.Property<bool?>("is_suspended");

                    b.Property<string>("phone");

                    b.Property<int?>("providerid");

                    b.Property<DateTime>("reg_date");

                    b.Property<string>("surname");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.HasIndex("providerid");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("royaltyevents.Models.Booking", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("occussionid");

                    b.Property<int?>("serviceid");

                    b.Property<string>("specs");

                    b.Property<int>("stage");

                    b.HasKey("id");

                    b.HasIndex("occussionid");

                    b.HasIndex("serviceid");

                    b.ToTable("Bookings");
                });

            modelBuilder.Entity("royaltyevents.Models.BotSetting", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("bot_setting_type");

                    b.Property<string>("exlusions");

                    b.Property<string>("language");

                    b.Property<string>("positions");

                    b.Property<int>("similar_id");

                    b.Property<string>("translation");

                    b.Property<string>("value");

                    b.HasKey("id");

                    b.ToTable("BotSettings");
                });

            modelBuilder.Entity("royaltyevents.Models.Event", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("description");

                    b.Property<DateTime>("end_date");

                    b.Property<int?>("event_typeid");

                    b.Property<string>("name");

                    b.Property<string>("primary_color");

                    b.Property<string>("secondary_color");

                    b.Property<DateTime>("start_date");

                    b.Property<string>("userId");

                    b.Property<string>("venue");

                    b.HasKey("id");

                    b.HasIndex("event_typeid");

                    b.HasIndex("userId");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("royaltyevents.Models.EventType", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("name");

                    b.HasKey("id");

                    b.ToTable("EventTypes");
                });

            modelBuilder.Entity("royaltyevents.Models.EventTypePackage", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("class_level");

                    b.Property<int?>("event_typeid");

                    b.Property<string>("max_price");

                    b.Property<string>("min_price");

                    b.Property<int?>("service_categoryid");

                    b.HasKey("id");

                    b.HasIndex("event_typeid");

                    b.HasIndex("service_categoryid");

                    b.ToTable("EventTypePackages");
                });

            modelBuilder.Entity("royaltyevents.Models.Following", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("date");

                    b.Property<int?>("providerid");

                    b.Property<string>("userid");

                    b.HasKey("id");

                    b.HasIndex("providerid");

                    b.ToTable("Followings");
                });

            modelBuilder.Entity("royaltyevents.Models.Gallery", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ff_file_url");

                    b.Property<string>("file_url");

                    b.Property<int?>("threadid");

                    b.Property<int>("type");

                    b.HasKey("id");

                    b.HasIndex("threadid");

                    b.ToTable("Gallery");
                });

            modelBuilder.Entity("royaltyevents.Models.Like", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("date");

                    b.Property<int?>("providerid");

                    b.Property<string>("userid");

                    b.HasKey("id");

                    b.HasIndex("providerid");

                    b.ToTable("Likes");
                });

            modelBuilder.Entity("royaltyevents.Models.Payment", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("amount");

                    b.Property<DateTime>("date");

                    b.Property<int>("payedforid");

                    b.Property<int>("paying_for");

                    b.Property<int?>("serviceid");

                    b.Property<DateTime>("validuntil");

                    b.HasKey("id");

                    b.HasIndex("serviceid");

                    b.ToTable("Payments");
                });

            modelBuilder.Entity("royaltyevents.Models.Provider", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("banner_url");

                    b.Property<string>("banner_url_ff");

                    b.Property<string>("bg_url");

                    b.Property<string>("bgurl_ff");

                    b.Property<string>("city");

                    b.Property<string>("country");

                    b.Property<string>("email");

                    b.Property<string>("facebook");

                    b.Property<string>("field");

                    b.Property<string>("instagram");

                    b.Property<string>("intro_video_url");

                    b.Property<string>("intro_video_url_ff");

                    b.Property<string>("lat_long");

                    b.Property<string>("name");

                    b.Property<string>("phone");

                    b.Property<string>("pintrest");

                    b.Property<string>("primary_color");

                    b.Property<string>("province");

                    b.Property<string>("radius");

                    b.Property<DateTime>("reg_date");

                    b.Property<string>("reg_no");

                    b.Property<string>("secondary_color");

                    b.Property<string>("site_url");

                    b.Property<string>("street_address");

                    b.Property<string>("twitter");

                    b.HasKey("id");

                    b.ToTable("Providers");
                });

            modelBuilder.Entity("royaltyevents.Models.Reception", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("is_read");

                    b.Property<int?>("threadid");

                    b.Property<string>("userid");

                    b.HasKey("id");

                    b.HasIndex("threadid");

                    b.ToTable("Reception");
                });

            modelBuilder.Entity("royaltyevents.Models.Schedule", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("description");

                    b.Property<DateTime>("end_date");

                    b.Property<int?>("occussionid");

                    b.Property<string>("primary_color");

                    b.Property<string>("secondary_color");

                    b.Property<DateTime>("start_date");

                    b.Property<string>("title");

                    b.HasKey("id");

                    b.HasIndex("occussionid");

                    b.ToTable("Schedules");
                });

            modelBuilder.Entity("royaltyevents.Models.Service", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("categoryid");

                    b.Property<decimal>("deposit");

                    b.Property<string>("description");

                    b.Property<string>("disclaimer");

                    b.Property<int>("image1_id");

                    b.Property<int>("image2_id");

                    b.Property<int>("image3_id");

                    b.Property<int>("image4_id");

                    b.Property<int>("image5_id");

                    b.Property<int>("image6_id");

                    b.Property<string>("name");

                    b.Property<decimal>("price");

                    b.Property<int?>("providerid");

                    b.Property<string>("terms_and_conditions");

                    b.Property<string>("title");

                    b.HasKey("id");

                    b.HasIndex("categoryid");

                    b.HasIndex("providerid");

                    b.ToTable("Service");
                });

            modelBuilder.Entity("royaltyevents.Models.ServiceCategory", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("name");

                    b.HasKey("id");

                    b.ToTable("ServiceCategories");
                });

            modelBuilder.Entity("royaltyevents.Models.Thread", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("content");

                    b.Property<DateTime>("date");

                    b.Property<int>("image_id");

                    b.Property<bool>("read_status");

                    b.Property<int?>("sender_providerid");

                    b.Property<string>("sender_userId");

                    b.Property<int>("target_group");

                    b.Property<int?>("target_providerid");

                    b.Property<string>("target_userid");

                    b.Property<string>("title");

                    b.Property<int>("type");

                    b.Property<int>("video_id");

                    b.HasKey("id");

                    b.HasIndex("sender_providerid");

                    b.HasIndex("sender_userId");

                    b.ToTable("Threads");
                });

            modelBuilder.Entity("royaltyevents.Models.View", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("date");

                    b.Property<int?>("providerid");

                    b.HasKey("id");

                    b.HasIndex("providerid");

                    b.ToTable("Views");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("royaltyevents.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("royaltyevents.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("royaltyevents.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("royaltyevents.Models.ApplicationUser", b =>
                {
                    b.HasOne("royaltyevents.Models.Provider", "provider")
                        .WithMany("users")
                        .HasForeignKey("providerid");
                });

            modelBuilder.Entity("royaltyevents.Models.Booking", b =>
                {
                    b.HasOne("royaltyevents.Models.Event", "occussion")
                        .WithMany()
                        .HasForeignKey("occussionid");

                    b.HasOne("royaltyevents.Models.Service", "service")
                        .WithMany()
                        .HasForeignKey("serviceid");
                });

            modelBuilder.Entity("royaltyevents.Models.Event", b =>
                {
                    b.HasOne("royaltyevents.Models.EventType", "event_type")
                        .WithMany()
                        .HasForeignKey("event_typeid");

                    b.HasOne("royaltyevents.Models.ApplicationUser", "user")
                        .WithMany("events")
                        .HasForeignKey("userId");
                });

            modelBuilder.Entity("royaltyevents.Models.EventTypePackage", b =>
                {
                    b.HasOne("royaltyevents.Models.EventType", "event_type")
                        .WithMany()
                        .HasForeignKey("event_typeid");

                    b.HasOne("royaltyevents.Models.ServiceCategory", "service_category")
                        .WithMany()
                        .HasForeignKey("service_categoryid");
                });

            modelBuilder.Entity("royaltyevents.Models.Following", b =>
                {
                    b.HasOne("royaltyevents.Models.Provider", "provider")
                        .WithMany()
                        .HasForeignKey("providerid");
                });

            modelBuilder.Entity("royaltyevents.Models.Gallery", b =>
                {
                    b.HasOne("royaltyevents.Models.Thread", "thread")
                        .WithMany("gallery")
                        .HasForeignKey("threadid");
                });

            modelBuilder.Entity("royaltyevents.Models.Like", b =>
                {
                    b.HasOne("royaltyevents.Models.Provider", "provider")
                        .WithMany()
                        .HasForeignKey("providerid");
                });

            modelBuilder.Entity("royaltyevents.Models.Payment", b =>
                {
                    b.HasOne("royaltyevents.Models.Service", "service")
                        .WithMany("payments")
                        .HasForeignKey("serviceid");
                });

            modelBuilder.Entity("royaltyevents.Models.Reception", b =>
                {
                    b.HasOne("royaltyevents.Models.Thread", "thread")
                        .WithMany()
                        .HasForeignKey("threadid");
                });

            modelBuilder.Entity("royaltyevents.Models.Schedule", b =>
                {
                    b.HasOne("royaltyevents.Models.Event", "occussion")
                        .WithMany("schedules")
                        .HasForeignKey("occussionid");
                });

            modelBuilder.Entity("royaltyevents.Models.Service", b =>
                {
                    b.HasOne("royaltyevents.Models.ServiceCategory", "category")
                        .WithMany("services")
                        .HasForeignKey("categoryid");

                    b.HasOne("royaltyevents.Models.Provider", "provider")
                        .WithMany()
                        .HasForeignKey("providerid");
                });

            modelBuilder.Entity("royaltyevents.Models.Thread", b =>
                {
                    b.HasOne("royaltyevents.Models.Provider", "sender_provider")
                        .WithMany()
                        .HasForeignKey("sender_providerid");

                    b.HasOne("royaltyevents.Models.ApplicationUser", "sender_user")
                        .WithMany()
                        .HasForeignKey("sender_userId");
                });

            modelBuilder.Entity("royaltyevents.Models.View", b =>
                {
                    b.HasOne("royaltyevents.Models.Provider", "provider")
                        .WithMany()
                        .HasForeignKey("providerid");
                });
        }
    }
}
