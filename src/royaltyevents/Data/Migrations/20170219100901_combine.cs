﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace royaltyevents.Data.Migrations
{
    public partial class combine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Threads_AspNetUsers_senderId",
                table: "Threads");

            migrationBuilder.DropIndex(
                name: "IX_Threads_senderId",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "senderId",
                table: "Threads");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "senderId",
                table: "Threads",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Threads_senderId",
                table: "Threads",
                column: "senderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Threads_AspNetUsers_senderId",
                table: "Threads",
                column: "senderId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
