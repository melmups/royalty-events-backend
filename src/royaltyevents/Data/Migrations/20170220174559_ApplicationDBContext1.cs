﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace royaltyevents.Data.Migrations
{
    public partial class ApplicationDBContext1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    avator = table.Column<string>(nullable: true),
                    city = table.Column<string>(nullable: true),
                    daysleft = table.Column<int>(nullable: false),
                    email = table.Column<string>(nullable: true),
                    eventname = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    province = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.id);
                });

            migrationBuilder.AddColumn<string>(
                name: "facebook",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "instagram",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pintrest",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "twitter",
                table: "Providers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "facebook",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "instagram",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "pintrest",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "twitter",
                table: "Providers");

            migrationBuilder.DropTable(
                name: "Client");
        }
    }
}
