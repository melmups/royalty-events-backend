﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace royaltyevents.Data.Migrations
{
    public partial class stage1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Followings_AspNetUsers_userId",
                table: "Followings");

            migrationBuilder.DropForeignKey(
                name: "FK_Likes_AspNetUsers_userId",
                table: "Likes");

            migrationBuilder.DropForeignKey(
                name: "FK_Views_AspNetUsers_userId",
                table: "Views");

            migrationBuilder.DropIndex(
                name: "IX_Views_userId",
                table: "Views");

            migrationBuilder.DropIndex(
                name: "IX_Likes_userId",
                table: "Likes");

            migrationBuilder.DropIndex(
                name: "IX_Followings_userId",
                table: "Followings");

            migrationBuilder.DropColumn(
                name: "userId",
                table: "Views");

            migrationBuilder.DropColumn(
                name: "thread_id",
                table: "Reception");

            migrationBuilder.AddColumn<string>(
                name: "userid",
                table: "Reception",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "userId",
                table: "Events",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "avator_url",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "country",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "is_admin",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "is_client",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "is_provider",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "is_suspended",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "phone",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "providerid",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "reg_date",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "surname",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Likes",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Followings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Events_userId",
                table: "Events",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_providerid",
                table: "AspNetUsers",
                column: "providerid");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Providers_providerid",
                table: "AspNetUsers",
                column: "providerid",
                principalTable: "Providers",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_AspNetUsers_userId",
                table: "Events",
                column: "userId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.RenameColumn(
                name: "userId",
                table: "Likes",
                newName: "userid");

            migrationBuilder.RenameColumn(
                name: "userId",
                table: "Followings",
                newName: "userid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Providers_providerid",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Events_AspNetUsers_userId",
                table: "Events");

            migrationBuilder.DropIndex(
                name: "IX_Events_userId",
                table: "Events");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_providerid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "userid",
                table: "Reception");

            migrationBuilder.DropColumn(
                name: "userId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "avator_url",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "country",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "is_admin",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "is_client",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "is_provider",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "is_suspended",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "phone",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "providerid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "reg_date",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "surname",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "userId",
                table: "Views",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "thread_id",
                table: "Reception",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Views_userId",
                table: "Views",
                column: "userId");

            migrationBuilder.AlterColumn<string>(
                name: "userid",
                table: "Likes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Likes_userId",
                table: "Likes",
                column: "userid");

            migrationBuilder.AlterColumn<string>(
                name: "userid",
                table: "Followings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Followings_userId",
                table: "Followings",
                column: "userid");

            migrationBuilder.AddForeignKey(
                name: "FK_Followings_AspNetUsers_userId",
                table: "Followings",
                column: "userid",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_AspNetUsers_userId",
                table: "Likes",
                column: "userid",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Views_AspNetUsers_userId",
                table: "Views",
                column: "userId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.RenameColumn(
                name: "userid",
                table: "Likes",
                newName: "userId");

            migrationBuilder.RenameColumn(
                name: "userid",
                table: "Followings",
                newName: "userId");
        }
    }
}
