﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace royaltyevents.Data.Migrations
{
    public partial class InitialSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BotSettings",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    bot_setting_type = table.Column<int>(nullable: false),
                    exlusions = table.Column<string>(nullable: true),
                    language = table.Column<string>(nullable: true),
                    positions = table.Column<string>(nullable: true),
                    similar_id = table.Column<int>(nullable: false),
                    translation = table.Column<string>(nullable: true),
                    value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotSettings", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "EventTypes",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventTypes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Providers",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    banner_url = table.Column<string>(nullable: true),
                    bg_url = table.Column<string>(nullable: true),
                    city = table.Column<string>(nullable: true),
                    country = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    ff_banner_url = table.Column<string>(nullable: true),
                    intro_video_url = table.Column<string>(nullable: true),
                    lat_long = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    primary_color = table.Column<string>(nullable: true),
                    province = table.Column<string>(nullable: true),
                    radius = table.Column<string>(nullable: true),
                    reg_date = table.Column<DateTime>(nullable: false),
                    reg_no = table.Column<string>(nullable: true),
                    secondary_color = table.Column<string>(nullable: true),
                    site_url = table.Column<string>(nullable: true),
                    street_address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Providers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ServiceCategories",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceCategories", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Threads",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    content = table.Column<string>(nullable: true),
                    date = table.Column<DateTime>(nullable: false),
                    image_id = table.Column<int>(nullable: false),
                    read_status = table.Column<bool>(nullable: false),
                    senderId = table.Column<string>(nullable: true),
                    sender_id = table.Column<int>(nullable: false),
                    target_group = table.Column<int>(nullable: false),
                    target_id = table.Column<int>(nullable: false),
                    type = table.Column<int>(nullable: false),
                    video_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Threads", x => x.id);
                    table.ForeignKey(
                        name: "FK_Threads_AspNetUsers_senderId",
                        column: x => x.senderId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    date = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    event_typeid = table.Column<int>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    primary_color = table.Column<string>(nullable: true),
                    secondary_color = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.id);
                    table.ForeignKey(
                        name: "FK_Events_EventTypes_event_typeid",
                        column: x => x.event_typeid,
                        principalTable: "EventTypes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Followings",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    date = table.Column<DateTime>(nullable: false),
                    providerid = table.Column<int>(nullable: true),
                    userId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Followings", x => x.id);
                    table.ForeignKey(
                        name: "FK_Followings_Providers_providerid",
                        column: x => x.providerid,
                        principalTable: "Providers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Followings_AspNetUsers_userId",
                        column: x => x.userId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Likes",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    date = table.Column<DateTime>(nullable: false),
                    providerid = table.Column<int>(nullable: true),
                    userId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Likes", x => x.id);
                    table.ForeignKey(
                        name: "FK_Likes_Providers_providerid",
                        column: x => x.providerid,
                        principalTable: "Providers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Likes_AspNetUsers_userId",
                        column: x => x.userId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Views",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    date = table.Column<DateTime>(nullable: false),
                    providerid = table.Column<int>(nullable: true),
                    userId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Views", x => x.id);
                    table.ForeignKey(
                        name: "FK_Views_Providers_providerid",
                        column: x => x.providerid,
                        principalTable: "Providers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Views_AspNetUsers_userId",
                        column: x => x.userId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EventTypePackages",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    class_level = table.Column<string>(nullable: true),
                    event_typeid = table.Column<int>(nullable: true),
                    max_price = table.Column<string>(nullable: true),
                    min_price = table.Column<string>(nullable: true),
                    service_categoryid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventTypePackages", x => x.id);
                    table.ForeignKey(
                        name: "FK_EventTypePackages_EventTypes_event_typeid",
                        column: x => x.event_typeid,
                        principalTable: "EventTypes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EventTypePackages_ServiceCategories_service_categoryid",
                        column: x => x.service_categoryid,
                        principalTable: "ServiceCategories",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Service",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    categoryid = table.Column<int>(nullable: true),
                    deposit = table.Column<decimal>(nullable: false),
                    description = table.Column<int>(nullable: false),
                    disclaimer = table.Column<string>(nullable: true),
                    image1_id = table.Column<int>(nullable: false),
                    image2_id = table.Column<int>(nullable: false),
                    image3_id = table.Column<int>(nullable: false),
                    image4_id = table.Column<int>(nullable: false),
                    image5_id = table.Column<int>(nullable: false),
                    image6_id = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    price = table.Column<decimal>(nullable: false),
                    providerid = table.Column<int>(nullable: true),
                    terms_and_conditions = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Service", x => x.id);
                    table.ForeignKey(
                        name: "FK_Service_ServiceCategories_categoryid",
                        column: x => x.categoryid,
                        principalTable: "ServiceCategories",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Service_Providers_providerid",
                        column: x => x.providerid,
                        principalTable: "Providers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Gallery",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ff_file_url = table.Column<string>(nullable: true),
                    file_url = table.Column<string>(nullable: true),
                    threadid = table.Column<int>(nullable: true),
                    type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gallery", x => x.id);
                    table.ForeignKey(
                        name: "FK_Gallery_Threads_threadid",
                        column: x => x.threadid,
                        principalTable: "Threads",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reception",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    is_read = table.Column<bool>(nullable: false),
                    thread_id = table.Column<int>(nullable: false),
                    threadid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reception", x => x.id);
                    table.ForeignKey(
                        name: "FK_Reception_Threads_threadid",
                        column: x => x.threadid,
                        principalTable: "Threads",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Schedules",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    color = table.Column<string>(nullable: true),
                    draggable = table.Column<bool>(nullable: false),
                    end_date = table.Column<DateTime>(nullable: false),
                    fluid_end = table.Column<bool>(nullable: false),
                    fluid_start = table.Column<bool>(nullable: false),
                    occussionid = table.Column<int>(nullable: true),
                    start_date = table.Column<DateTime>(nullable: false),
                    title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedules", x => x.id);
                    table.ForeignKey(
                        name: "FK_Schedules_Events_occussionid",
                        column: x => x.occussionid,
                        principalTable: "Events",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Bookings",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    occussionid = table.Column<int>(nullable: true),
                    serviceid = table.Column<int>(nullable: true),
                    specs = table.Column<string>(nullable: true),
                    stage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookings", x => x.id);
                    table.ForeignKey(
                        name: "FK_Bookings_Events_occussionid",
                        column: x => x.occussionid,
                        principalTable: "Events",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Bookings_Service_serviceid",
                        column: x => x.serviceid,
                        principalTable: "Service",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    amount = table.Column<decimal>(nullable: false),
                    date = table.Column<DateTime>(nullable: false),
                    paying_for = table.Column<int>(nullable: false),
                    serviceid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.id);
                    table.ForeignKey(
                        name: "FK_Payments_Service_serviceid",
                        column: x => x.serviceid,
                        principalTable: "Service",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_occussionid",
                table: "Bookings",
                column: "occussionid");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_serviceid",
                table: "Bookings",
                column: "serviceid");

            migrationBuilder.CreateIndex(
                name: "IX_Events_event_typeid",
                table: "Events",
                column: "event_typeid");

            migrationBuilder.CreateIndex(
                name: "IX_EventTypePackages_event_typeid",
                table: "EventTypePackages",
                column: "event_typeid");

            migrationBuilder.CreateIndex(
                name: "IX_EventTypePackages_service_categoryid",
                table: "EventTypePackages",
                column: "service_categoryid");

            migrationBuilder.CreateIndex(
                name: "IX_Followings_providerid",
                table: "Followings",
                column: "providerid");

            migrationBuilder.CreateIndex(
                name: "IX_Followings_userId",
                table: "Followings",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Gallery_threadid",
                table: "Gallery",
                column: "threadid");

            migrationBuilder.CreateIndex(
                name: "IX_Likes_providerid",
                table: "Likes",
                column: "providerid");

            migrationBuilder.CreateIndex(
                name: "IX_Likes_userId",
                table: "Likes",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_serviceid",
                table: "Payments",
                column: "serviceid");

            migrationBuilder.CreateIndex(
                name: "IX_Reception_threadid",
                table: "Reception",
                column: "threadid");

            migrationBuilder.CreateIndex(
                name: "IX_Schedules_occussionid",
                table: "Schedules",
                column: "occussionid");

            migrationBuilder.CreateIndex(
                name: "IX_Service_categoryid",
                table: "Service",
                column: "categoryid");

            migrationBuilder.CreateIndex(
                name: "IX_Service_providerid",
                table: "Service",
                column: "providerid");

            migrationBuilder.CreateIndex(
                name: "IX_Threads_senderId",
                table: "Threads",
                column: "senderId");

            migrationBuilder.CreateIndex(
                name: "IX_Views_providerid",
                table: "Views",
                column: "providerid");

            migrationBuilder.CreateIndex(
                name: "IX_Views_userId",
                table: "Views",
                column: "userId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookings");

            migrationBuilder.DropTable(
                name: "BotSettings");

            migrationBuilder.DropTable(
                name: "EventTypePackages");

            migrationBuilder.DropTable(
                name: "Followings");

            migrationBuilder.DropTable(
                name: "Gallery");

            migrationBuilder.DropTable(
                name: "Likes");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "Reception");

            migrationBuilder.DropTable(
                name: "Schedules");

            migrationBuilder.DropTable(
                name: "Views");

            migrationBuilder.DropTable(
                name: "Service");

            migrationBuilder.DropTable(
                name: "Threads");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "ServiceCategories");

            migrationBuilder.DropTable(
                name: "Providers");

            migrationBuilder.DropTable(
                name: "EventTypes");
        }
    }
}
