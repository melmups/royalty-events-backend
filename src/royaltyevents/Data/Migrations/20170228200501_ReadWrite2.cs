﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace royaltyevents.Data.Migrations
{
    public partial class ReadWrite2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "image_id",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "read_status",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "video_id",
                table: "Threads");

            migrationBuilder.DropTable(
                name: "Reception");

            migrationBuilder.CreateTable(
                name: "Recepients",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    is_read = table.Column<bool>(nullable: false),
                    providerid = table.Column<int>(nullable: false),
                    threadid = table.Column<int>(nullable: true),
                    userid = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recepients", x => x.id);
                    table.ForeignKey(
                        name: "FK_Recepients_Threads_threadid",
                        column: x => x.threadid,
                        principalTable: "Threads",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.AddColumn<string>(
                name: "image_url",
                table: "Threads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "image_url_ff",
                table: "Threads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "video_url",
                table: "Threads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "video_url_ff",
                table: "Threads",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Recepients_threadid",
                table: "Recepients",
                column: "threadid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "image_url",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "image_url_ff",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "video_url",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "video_url_ff",
                table: "Threads");

            migrationBuilder.DropTable(
                name: "Recepients");

            migrationBuilder.CreateTable(
                name: "Reception",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    is_read = table.Column<bool>(nullable: false),
                    threadid = table.Column<int>(nullable: true),
                    userid = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reception", x => x.id);
                    table.ForeignKey(
                        name: "FK_Reception_Threads_threadid",
                        column: x => x.threadid,
                        principalTable: "Threads",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.AddColumn<int>(
                name: "image_id",
                table: "Threads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "read_status",
                table: "Threads",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "video_id",
                table: "Threads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Reception_threadid",
                table: "Reception",
                column: "threadid");
        }
    }
}
