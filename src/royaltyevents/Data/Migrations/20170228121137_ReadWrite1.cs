﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace royaltyevents.Data.Migrations
{
    public partial class ReadWrite1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "color",
                table: "Schedules");

            migrationBuilder.DropColumn(
                name: "draggable",
                table: "Schedules");

            migrationBuilder.DropColumn(
                name: "fluid_end",
                table: "Schedules");

            migrationBuilder.DropColumn(
                name: "fluid_start",
                table: "Schedules");

            migrationBuilder.DropColumn(
                name: "date",
                table: "Events");

            migrationBuilder.AddColumn<string>(
                name: "description",
                table: "Schedules",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "primary_color",
                table: "Schedules",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "secondary_color",
                table: "Schedules",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "end_date",
                table: "Events",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "start_date",
                table: "Events",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "description",
                table: "Schedules");

            migrationBuilder.DropColumn(
                name: "primary_color",
                table: "Schedules");

            migrationBuilder.DropColumn(
                name: "secondary_color",
                table: "Schedules");

            migrationBuilder.DropColumn(
                name: "end_date",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "start_date",
                table: "Events");

            migrationBuilder.AddColumn<string>(
                name: "color",
                table: "Schedules",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "draggable",
                table: "Schedules",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "fluid_end",
                table: "Schedules",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "fluid_start",
                table: "Schedules",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "date",
                table: "Events",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
