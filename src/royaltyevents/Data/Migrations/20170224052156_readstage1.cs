﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace royaltyevents.Data.Migrations
{
    public partial class readstage1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "sender_id",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "target_id",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "ff_banner_url",
                table: "Providers");

            migrationBuilder.DropTable(
                name: "Client");

            migrationBuilder.AddColumn<int>(
                name: "sender_providerid",
                table: "Threads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sender_userId",
                table: "Threads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "target_providerid",
                table: "Threads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "target_userid",
                table: "Threads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "title",
                table: "Threads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "title",
                table: "Service",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "banner_url_ff",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "bgurl_ff",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "field",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "intro_video_url_ff",
                table: "Providers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "payedforid",
                table: "Payments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "validuntil",
                table: "Payments",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "venue",
                table: "Events",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Threads_sender_providerid",
                table: "Threads",
                column: "sender_providerid");

            migrationBuilder.CreateIndex(
                name: "IX_Threads_sender_userId",
                table: "Threads",
                column: "sender_userId");

            migrationBuilder.AlterColumn<string>(
                name: "description",
                table: "Service",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Threads_Providers_sender_providerid",
                table: "Threads",
                column: "sender_providerid",
                principalTable: "Providers",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Threads_AspNetUsers_sender_userId",
                table: "Threads",
                column: "sender_userId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Threads_Providers_sender_providerid",
                table: "Threads");

            migrationBuilder.DropForeignKey(
                name: "FK_Threads_AspNetUsers_sender_userId",
                table: "Threads");

            migrationBuilder.DropIndex(
                name: "IX_Threads_sender_providerid",
                table: "Threads");

            migrationBuilder.DropIndex(
                name: "IX_Threads_sender_userId",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "sender_providerid",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "sender_userId",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "target_providerid",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "target_userid",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "title",
                table: "Threads");

            migrationBuilder.DropColumn(
                name: "title",
                table: "Service");

            migrationBuilder.DropColumn(
                name: "banner_url_ff",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "bgurl_ff",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "field",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "intro_video_url_ff",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "payedforid",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "validuntil",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "venue",
                table: "Events");

            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    avator = table.Column<string>(nullable: true),
                    city = table.Column<string>(nullable: true),
                    daysleft = table.Column<int>(nullable: false),
                    email = table.Column<string>(nullable: true),
                    eventname = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    province = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.id);
                });

            migrationBuilder.AddColumn<int>(
                name: "sender_id",
                table: "Threads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "target_id",
                table: "Threads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ff_banner_url",
                table: "Providers",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "description",
                table: "Service",
                nullable: false);
        }
    }
}
