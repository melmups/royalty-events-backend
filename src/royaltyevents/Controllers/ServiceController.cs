using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Services;
using royaltyevents.Models;

namespace royaltyevents.Controllers
{
    [Produces("application/json")]
 
    public class ServiceController : Controller
    {
        IRepository<ServiceViewModel> Services;
        IRepository<PaymentViewModel> _Payment;
        public ServiceController(IRepository<ServiceViewModel> providerservices, IRepository<PaymentViewModel> payment)
        {
            Services = providerservices;
            _Payment = payment;
        }

        // GET: api/Service/5
        [HttpGet("{id}")]
        [Route("api/service/{id}")]
        public IEnumerable<ServiceViewModel> Get(int id)
        {

            return Services.GetAll(id);
        }
        
        // POST: api/Service
        [HttpPost]
        [Route("api/service")]
        public IActionResult Post([FromBody]ServiceViewModel service)
        {
            if (ModelState.IsValid)
            {
                Services.Add(service);
                return StatusCode(201);
            }
            //return the created at status
            return BadRequest(ModelState);
        }

        // PUT: api/Service/5
        [HttpPut("{id}")]
        [Route("api/[controller]")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Route("api/[controller]")]
        public void Delete(int id)
        {
        }
        [HttpPost]
        [Route("api/service/pay")]
        public IActionResult Pay([FromBody]PaymentViewModel payment)
        {
            if (ModelState.IsValid)
            {
                //modify payment view type to be of type service
                payment.paying_for = "service";
                _Payment.Add(payment);
                return StatusCode(201);
            }
            return BadRequest(ModelState);
        }
    }
}
