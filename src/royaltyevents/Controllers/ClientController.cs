using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using royaltyevents.Services;
using royaltyevents.Models.ClientViewModels;
namespace royaltyevents.Controllers
{
    [Produces("application/json")]
    [Route("api/Client")]
    public class ClientController : Controller
    {
        IRepository<ClientsViewModel> Client;

        public ClientController(IRepository<ClientsViewModel> client)
        {
            Client = client;
        }
        // GET: api/Client
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Client/5
        [HttpGet("{id}")]
        public IEnumerable<ClientsViewModel> Get(int id)
        {
            return Client.GetAll(id);
        }
        
        // POST: api/Client
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Client/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
