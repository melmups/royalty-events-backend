using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using royaltyevents.Services;
using royaltyevents.Models.ClientViewModels;

namespace royaltyevents.Controllers
{
    [Produces("application/json")]
    [Route("api/Events")]
    public class EventsController : Controller
    {
        IRepository<EventViewModel> Events;
        public EventsController(IRepository<EventViewModel> eventrepository)
        {
            Events = eventrepository;
        }
        // GET: api/Events
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Events/5
        [HttpGet("{id}")]
        public IEnumerable<EventViewModel> Get(int id)
        {
            return Events.GetAll(id);
        }
        
        // POST: api/Events
        [HttpPost]
        public IActionResult Post([FromBody]EventViewModel schedule)
        {
            //validate
            if (ModelState.IsValid)
            {
                Events.Add(schedule);
                return StatusCode(201);
            }

            return BadRequest(ModelState);
        }
        
        // PUT: api/Events/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
