﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace royaltyevents.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            //SPA page for the client
            return View();
        }
        public IActionResult Admin()
        {
            //SPA page for adminstration
            return View();
        }

    }
}
