using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Services;
using Microsoft.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace royaltyevents.Controllers
{
    [Produces("application/json")]
  
    public class ProfileController : Controller
    {
        IRepository<UserProfileViewModel> Profiles ;
       
        public ProfileController(IRepository<UserProfileViewModel> profileRepository)
        {
            Profiles = profileRepository;
          
        }
        
        // GET: api/Profile
        [HttpGet]
        [Authorize]
        [Route("api/[controller]")]
        public object Get()
        {
            //Retrieve sub from clams and query database from existing database
            //return User.Claims.Select(c =>
            // new
            // {
            //     Type = c.Type,
            //     Value = c.Value
            // });

            //Retrieve sub from clams and query database from existing database

            var user_id = User.Claims.FirstOrDefault(x => x.Type == "user_id");
            if (user_id.Value == "google-oauth2|10565961829038420937")
            {
                //Take user information from the database using userid from Claims

                //Return single user from the userProfile Repository
                List<Message> messages = new List<Message>()
                {
                   new Message {
                        content = "message 1",
                        time = Convert.ToDateTime("2017/1/1"),
                        is_read = false,
                        sender_userid = "33",
                        name = "albeit"

                    },
                   new Message
                   {
                       content = "message 1",
                       time = Convert.ToDateTime("2017/1/1"),
                       is_read = false,
                       sender_userid = "33",
                       name = "albeit"
                   }
                };
                List<Notification> notifications = new List<Notification>
                {
                    new Notification
                    {
                       title = "recent notification",
                       name = "Nyasha",
                       sender_userid = "12",
                       is_read = false,

                    }
                };

                var user = new UserProfileViewModel
                {
                    name = User.Claims.FirstOrDefault(x => x.Type == "name").Value,
                    email = User.Claims.FirstOrDefault(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").Value,
                    avator_url = User.Claims.FirstOrDefault(x => x.Type == "picture").Value,
                    is_client = true,
                    is_provider = false,
                    messages = messages,
                    notifications = notifications,
                    setup_stage = "complete"
                };

                return user;


            }

            var newuser = new UserProfileViewModel
            {
                name = "Redeemed",
                setup_stage = "register",
            };

            return newuser;

        }

        // GET: api/Profile
        [HttpGet]
       
        [Route("api/providers")]
        public IEnumerable<UserProfileViewModel> Providers()
        {
            return Profiles.GetAll();
        }
        [Route("api/providers/{id}")]
        public IEnumerable<UserProfileViewModel> Provider(int id)
        {
            return Profiles.GetAll().Where(x=>x.providerid==id);

        }

        // GET: api/Profile/5
        [HttpGet("{id}")]
        [Route("api/[controller]")]
        public UserProfileViewModel Get(int id)
        {
            return Profiles.Get(id);
        }
        // POST: api/Profile
        [HttpPost]
        [Route("api/[controller]")]
        public void Post([FromBody] UserProfileViewModel profile)
        {

            Profiles.Add(profile);
            //Retuning the created status and the created profile id
        }

        // PUT: api/Profile/5
        [HttpPut("{id}")]
        [Route("api/[controller]")]
        public void Put(int id, [FromBody]string value)
        {

        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Route("api/[controller]")]
        public void Delete(int id)
        {
        }



    }
}
