using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using royaltyevents.Services;
using royaltyevents.Models.ClientViewModels;

namespace royaltyevents.Controllers
{
    [Produces("application/json")]
    [Route("api/Timeline")]
    public class TimelineController : Controller
    {
        IRepository<TimelineViewModel> Timeline;
        public TimelineController(IRepository<TimelineViewModel> timeline)
        {
            Timeline = timeline;
        }
        // GET: api/Timeline
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Timeline/5
        [HttpGet("{id}")]
        public IEnumerable<TimelineViewModel> Get(int id)
        {
            // return Timeline.GetAll(id);
            return new List<TimelineViewModel>
           {
               new TimelineViewModel
               {
                  id=1,
                  time="4hr ago",
                  title="added new product",
                  avator="../assets/img/faces/avatar.jpg",
                  providername="Anina Beno",
                  provider_id=1,
                  content="We will be giving stupp rolls this season Yadaaa!!!",
               },
               new TimelineViewModel
               {
                    id = 1,
                    provider_id = 1,
                    avator = "../assets/img/faces/avatar1.jpg",
                    content = "book now up to 30th in midlands and bulawayo and get 25% off!!!",
                    title ="just started a promotion",
                    providername ="Annita brenno",
                    time = "1d ago"
               }

           };

        }
        
        // POST: api/Timeline
        [HttpPost]
        public IActionResult Post([FromBody] TimelineViewModel timeline)
        {
            if (ModelState.IsValid)
            {
                Timeline.Add(timeline);
                return StatusCode(201);
            }

            return BadRequest(ModelState);
        }

        // PUT: api/Timeline/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
