using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using royaltyevents.Data;
using royaltyevents.Models;

namespace royaltyevents.Controllers
{
    [Produces("application/json")]
    [Route("api/Gallery")]
    public class GalleryController : Controller
    {
        ApplicationDbContext db;
        IHostingEnvironment env;
        public GalleryController(IHostingEnvironment envronment, ApplicationDbContext context)
        {
            env = envronment;
            db = context;
        }
       
        // GET: api/Gallery
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Gallery/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        //Uploading an image and returning the Url and the id
        [HttpPost]
        public IActionResult Post()
        {
            long size = 0;
            var files = Request.Form.Files;
            var file = files[0];
            
                var filename = ContentDispositionHeaderValue
                                .Parse(file.ContentDisposition)
                                .FileName
                                .Trim('"');
                var type = ContentDispositionHeaderValue
                                .Parse(file.ContentDisposition)
                                .DispositionType;

                filename = env.WebRootPath + $@"\images\{filename}";
                size += file.Length;
                using (FileStream fs = System.IO.File.Create(filename))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
            if (file != null)
            {
                //saving metadata to the database
                Gallery galleryItem = new Gallery()
                {
                    file_url = filename,
                    type = GalleryType.image,
                };
                db.Gallery.Add(galleryItem);
                db.SaveChanges();

                var item = db.Gallery.FirstOrDefault(x => x.file_url == galleryItem.file_url);
                return Json(item);
            }
            return BadRequest();
        
            }
          
        
        // PUT: api/Gallery/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        }


    }

