using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using royaltyevents.Models.ClientViewModels;
using royaltyevents.Services;

namespace royaltyevents.Controllers
{
    [Produces("application/json")]
    [Route("api/Booking")]
    public class BookingController : Controller
    {
        IRepository<BookingViewModel> Bookings;
        IRepository<PaymentViewModel> _Payment;
        public BookingController(IRepository<BookingViewModel> clientbookings, IRepository<PaymentViewModel> payment)
        {            
            Bookings = clientbookings;
            _Payment = payment;
        }
        // GET: api/Booking
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Booking/5
        [HttpGet("{id}")]
        public IEnumerable<BookingViewModel> Get(int id)
        {
            return Bookings.GetAll(id);
        }
        
        // POST: api/Booking
        [HttpPost]
        public IActionResult Post([FromBody]BookingViewModel booking)
        {
            if (ModelState.IsValid)
            {
                Bookings.Add(booking);
                return StatusCode(201);
            }
            return BadRequest(ModelState);

        }
        
        // PUT: api/Booking/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        public IActionResult Pay([FromBody]PaymentViewModel payment)
        {
            //modify payment view type to be of type service
            payment.paying_for = "booking";
            _Payment.Add(payment);
            return Json("Okay");
        }
    }
}
